package com.dekko.ishtiak;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@SpringBootApplication
public class DekkoApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DekkoApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {

	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.setConnectTimeout(Duration.ofMillis(5000)).setReadTimeout(Duration.ofMillis(500000)).build();
	}
	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return objectMapper;
	}


//	@Bean
//	public Jaxb2Marshaller marshaller(){
//		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
//		marshaller.setContextPath(JAXBContext.JAXB_CONTEXT_FACTORY);
//		marshaller.setMarshallerProperties(Map.of("com.sun.xml.bind.xmlHeaders","<?xml version=\"1.0\"?>", Marshaller.JAXB_FRAGMENT, false));
//		return marshaller;
//	}

}
