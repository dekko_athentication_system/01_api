package com.dekko.ishtiak.registration.application.port.in.dto.response;

import com.dekko.ishtiak.core.util.model.BaseEntity;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SendNotificationModel extends BaseEntity {

//    @NotNull(message = "Login Id is required")
//    @NotEmpty(message = "Login Id can not be empty")
    private String loginId;
    private String userName;
    private String mobileNo;
    private String email;
    private String[] recieverEmail;
    private String otp;
    private String resetPassword;
    private String otpStatus;
    private String otpVerified;
    private Date otpGeneratedOn;
    private Date otpExpirationTime;
    private String otpRequestBy;
    private Date otpRequestOn;

    @Override
    public String toString(){
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

}
