package com.dekko.ishtiak.registration.application.port.in.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SendEmailResponse{

	private String message;
	private Boolean sentEmail;

}
