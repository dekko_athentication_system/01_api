package com.dekko.ishtiak.security.application.port.in;


import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.core.util.response.CommonObjectResponseBody;
import com.dekko.ishtiak.security.application.port.in.dto.request.LoginRequest;

import java.util.Map;

public interface LoginFeatureUseCase {

    CommonObjectResponseBody userLogin(LoginRequest loginRequest) throws ExceptionHandlerUtil;
    CommonObjectResponseBody userLogout(LoginRequest loginRequest) throws ExceptionHandlerUtil;

    CommonObjectResponseBody userInfo(LoginRequest loginRequest) throws ExceptionHandlerUtil;

}
