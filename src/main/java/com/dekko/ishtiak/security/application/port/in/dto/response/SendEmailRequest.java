package com.dekko.ishtiak.registration.application.port.in.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SendEmailRequest {

	private String[] receiverEmail;
	private String mailSubject;
	private String messageText;

	private String loginId;
	private String email;
	private String mobileNo;

	private String purpose;
	private String purposeValue;

	private String username;
	private String otp;
	private String resetPassword;
	private String instituteName;
	private String instituteAddress;

}
