package com.dekko.ishtiak.security.application.port.in.dto.request;

import com.google.gson.GsonBuilder;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.*;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginRequest {

    @NotNull(message = "userId is required")
    @NotEmpty(message = "userId can not be empty")
    private String userId;

    private String password;

    private String roleOid;

    private String isLoggedIn;



}
