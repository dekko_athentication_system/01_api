package com.dekko.ishtiak.security.application.service;

import com.dekko.ishtiak.core.util.*;
import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.core.util.repository.UtilRepository;
import com.dekko.ishtiak.core.util.response.CommonObjectResponseBody;
import com.dekko.ishtiak.security.adapter.out.persistance.database.repository.LoginRepository;
import com.dekko.ishtiak.security.application.port.in.LoginFeatureUseCase;
import com.dekko.ishtiak.security.application.port.in.dto.request.LoginRequest;
import com.dekko.ishtiak.security.application.port.out.LoginPersistencePort;
import com.dekko.ishtiak.security.domain.Login;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.dekko.ishtiak.core.util.Constant.ACTIVE;
import static com.dekko.ishtiak.core.util.MessageCode.MSG_2018;

@RequiredArgsConstructor

@Service
@Slf4j
public class LoginService implements LoginFeatureUseCase {

    private final ModelMapper modelMapper;
    private final IdGenerator idGenerator;
    private final UtilRepository utilRepository;
    private final LoginPersistencePort loginPersistencePort;
    private final LoginRepository loginRepository;
    private final RestTemplate restTemplate;

    @Override
    public CommonObjectResponseBody userLogin(LoginRequest loginRequest) throws ExceptionHandlerUtil {
        Login login = new Login();
        int countLoginId = loginRepository.countLoginId(loginRequest);
        if (countLoginId == 1) {
            login = loginPersistencePort.userLogin(loginRequest);
            System.out.println("Obj " + login);
            if (login.getPassword().equals(loginRequest.getPassword()) && login.getIsLoggedIn().equals(loginRequest.getIsLoggedIn())) {
                int updateIsLoggedIn = loginPersistencePort.updateIsLoggedIn(login);
                if(updateIsLoggedIn == 0){
                    CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
                    response.setStatus(false);
                    response.setCode(ResponseCode.UNAUTHORIZED.getValue());
                    response.setMessageCode(MessageCode.MSG_2011.getCode());
                    response.setMessage(MessageCode.MSG_2011.getMessage());
                    return response;
                }
                CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
                response.setStatus(true);
                response.setCode(ResponseCode.OK.getValue());
                response.setMessageCode(MessageCode.MSG_2000.getCode());
                response.setMessage(MessageCode.MSG_2000.getMessage());
                return response;
            } else {
                CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
                response.setStatus(false);
                response.setCode(ResponseCode.UNAUTHORIZED.getValue());
                response.setMessageCode(MessageCode.MSG_2011.getCode());
                response.setMessage(MessageCode.MSG_2011.getMessage());
                return response;
            }
        } else {
            CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
            response.setStatus(false);
            response.setCode(ResponseCode.NOT_FOUND.getValue());
            response.setMessageCode(MessageCode.MSG_2010.getCode());
            response.setMessage(MessageCode.MSG_2010.getMessage());
            return response;
        }
    }

    @Override
    public CommonObjectResponseBody userInfo(LoginRequest loginRequest) throws ExceptionHandlerUtil {
        Login login = new Login();
        login = loginPersistencePort.userLogin(loginRequest);

        if (login.getUserId().equals(loginRequest.getUserId())) {
            CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
            response.setStatus(true);
            response.setCode(ResponseCode.OK.getValue());
            response.setMessageCode(MessageCode.MSG_2000.getCode());
            response.setMessage(MessageCode.MSG_2000.getMessage());
            return response;
        } else {
            CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
            response.setStatus(false);
            response.setCode(ResponseCode.UNAUTHORIZED.getValue());
            response.setMessageCode(MessageCode.MSG_2011.getCode());
            response.setMessage(MessageCode.MSG_2011.getMessage());
            return response;
        }
    }

    @Override
    public CommonObjectResponseBody userLogout(LoginRequest loginRequest) throws ExceptionHandlerUtil {
        int login = 0;
        int countLoginId = loginRepository.countLoginId(loginRequest);
        if (countLoginId == 1) {
            login = loginPersistencePort.countIsLoggedIn(loginRequest);
            if (login > 0) {
                int updateIsLoggedOut = loginPersistencePort.updateIsLoggedOut(loginRequest);
                if(updateIsLoggedOut == 0){
                    CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
                    response.setStatus(false);
                    response.setCode(ResponseCode.UNAUTHORIZED.getValue());
                    response.setMessageCode(MessageCode.MSG_2011.getCode());
                    response.setMessage(MessageCode.MSG_2011.getMessage());
                    return response;
                }
                CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
                response.setStatus(true);
                response.setCode(ResponseCode.OK.getValue());
                response.setMessageCode(MessageCode.MSG_2000.getCode());
                response.setMessage(MessageCode.MSG_2000.getMessage());
                return response;
            } else {
                CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
                response.setStatus(false);
                response.setCode(ResponseCode.UNAUTHORIZED.getValue());
                response.setMessageCode(MessageCode.MSG_2011.getCode());
                response.setMessage(MessageCode.MSG_2011.getMessage());
                return response;
            }
        } else {
            CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(login).build();
            response.setStatus(false);
            response.setCode(ResponseCode.NOT_FOUND.getValue());
            response.setMessageCode(MessageCode.MSG_2010.getCode());
            response.setMessage(MessageCode.MSG_2010.getMessage());
            return response;
        }
    }


//    private Login mapRequestToDomain(LoginRequest loginRequest, Map<String, String> params) throws ExceptionHandlerUtil {
//        Login existLoginh;
//        if (loginRequest.getOid() != null) {
//            existLoginh = loginPersistencePort.findByOid(loginRequest.getOid(), params);
//            loginRequest.setCreatedBy(existLoginh.getCreatedBy());
//            loginRequest.setCreatedOn(existLoginh.getCreatedOn());
//            loginRequest.setUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
//            loginRequest.setUpdatedBy(loginRequest.getUpdatedBy());
//        } else {
//            loginRequest.setCreatedBy(loginRequest.getCreatedBy());
//            loginRequest.setCreatedOn(loginRequest.getCreatedOn() == null ? Timestamp.valueOf(LocalDateTime.now()) : loginRequest.getCreatedOn());
//        }
//        Login login = modelMapper.map(loginRequest, Login.class);
//        return login;
//    }
}

