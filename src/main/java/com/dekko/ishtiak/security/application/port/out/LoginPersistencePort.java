package com.dekko.ishtiak.security.application.port.out;

import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.security.application.port.in.dto.request.LoginRequest;
import com.dekko.ishtiak.security.domain.Login;

import java.util.Map;

public interface LoginPersistencePort {
    Login userLogin(LoginRequest loginRequest) throws ExceptionHandlerUtil;
    int updateIsLoggedIn(Login login) throws ExceptionHandlerUtil;
    int updateIsLoggedOut(LoginRequest loginRequest) throws ExceptionHandlerUtil;
    int countIsLoggedIn(LoginRequest loginRequest) throws ExceptionHandlerUtil;
}
