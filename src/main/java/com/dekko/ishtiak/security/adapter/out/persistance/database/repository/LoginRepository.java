package com.dekko.ishtiak.security.adapter.out.persistance.database.repository;


import com.dekko.ishtiak.security.adapter.out.persistance.database.entity.LoginEntity;
import com.dekko.ishtiak.security.application.port.in.dto.request.LoginRequest;
import com.dekko.ishtiak.security.domain.Login;

import java.util.List;
import java.util.Map;


public interface LoginRepository {


    int countLoginId(LoginRequest loginRequest);

    LoginEntity getUserInfo(LoginRequest loginRequest);

    int updateIsLoggedIn(Login login);

    int updateIsLoggedOut(LoginRequest loginRequest);
    int countIsLoggedIn(LoginRequest loginRequest);
}
