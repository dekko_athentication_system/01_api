package com.dekko.ishtiak.security.adapter.out.persistance.database.entity;

import com.dekko.ishtiak.core.util.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponseEntity extends BaseEntity {

    private String districtNameEn;
    private String districtNameBn;

    private String loginTypeNameEn;
    private String loginTypeNameBn;
    // for OTP
    private String email;
    private String contactNo;
}
