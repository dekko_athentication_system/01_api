package com.dekko.ishtiak.security.adapter.out.persistance.database.repository;


import com.dekko.ishtiak.core.util.Table;
import com.dekko.ishtiak.security.adapter.out.persistance.database.entity.LoginEntity;
import com.dekko.ishtiak.security.application.port.in.dto.request.LoginRequest;
import com.dekko.ishtiak.security.domain.Login;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Repository
public class LoginRepositoryImpl implements LoginRepository {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public int countLoginId(LoginRequest loginRequest) {
        String sql = "select count(l.user_id) "
                + " from " + Table.LOGIN + " l "
                + " where 1 = 1 and l.user_id = '" + loginRequest.getUserId() + "'";
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public LoginEntity getUserInfo(LoginRequest loginRequest) {
        LoginEntity data = new LoginEntity();

        try {
            String sql = "SELECT l.oid, l.user_id, l.password, l.role_oid, l.is_logged_in, r.role_id , r.role_name_en , r.role_name_bn , r.menu_json " +
                    " from " + Table.LOGIN + " l " +
                    " LEFT JOIN " + Table.ROLE + " r ON r.oid = l.role_oid " +
                    " where l.user_id = '" + loginRequest.getUserId() + "'";

            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

            for (Map<String, Object> row : rows) {
                data.setOid((String) row.get("oid"));
                data.setUserId((String) row.get("user_id"));
                data.setPassword((String) row.get("password"));
                data.setRoleOid((String) row.get("role_oid"));
                data.setIsLoggedIn((String) row.get("is_logged_in"));

                data.setRoleId((String) row.get("role_id"));
                data.setRoleNameEn((String) row.get("role_name_en"));
                data.setRoleNameBn((String) row.get("role_name_bn"));
                data.setMenuJson((String) row.get("menu_json"));
                break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public int updateIsLoggedIn(Login login) {
        System.out.println(login);
        String sql = "UPDATE " + Table.LOGIN  +
                " SET is_logged_in = ? " +
                " where oid = ?";

        return jdbcTemplate.update(sql, "Yes", login.getOid());
    }

    @Override
    public int updateIsLoggedOut(LoginRequest loginRequest) {
        String sql = "UPDATE " + Table.LOGIN  +
                " SET is_logged_in = ? " +
                " where user_id = ?";

        return jdbcTemplate.update(sql, "No", loginRequest.getUserId());
    }

    @Override
    public int countIsLoggedIn(LoginRequest loginRequest) {
        String sql = "select count(l.is_logged_in)"
                + " from " + Table.LOGIN + " l "
                + " where 1 = 1 and l.is_logged_in = 'Yes' and l.user_id = '" + loginRequest.getUserId() + "'";
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }
}
