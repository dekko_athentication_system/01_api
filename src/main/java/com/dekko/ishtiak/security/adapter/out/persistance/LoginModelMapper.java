package com.dekko.ishtiak.security.adapter.out.persistance;

import com.dekko.ishtiak.security.adapter.out.persistance.database.entity.LoginEntity;
import com.dekko.ishtiak.security.domain.Login;
import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class LoginModelMapper {

    //    private final ObjectMapper objectMapper;
    private final ModelMapper modelMapper;

    public LoginModelMapper(
//            ObjectMapper objectMapper,
            ModelMapper modelMapper) {
//        this.objectMapper = objectMapper;
        this.modelMapper = modelMapper;
    }

    public Login mapToDomainEntity(LoginEntity loginEntity, Class<Login> loginClass) throws ExceptionHandlerUtil {
        /*modelMapper.typeMap(LoginEntity.class, Login.class)
                .addMappings(mapper -> mapper.map(LoginEntity::getBranchId, Login::setBranchId));
        modelMapper.typeMap(LoginEntity.class, Login.class)
                .addMappings(mapper -> mapper.map(LoginEntity::getOid, Login::setOid));*/
        Login login = modelMapper.map(loginEntity, Login.class);

//        try {
//            login.setStatus(objectMapper.readValue(loginEntity.getStatus(), String.class));
//        } catch (JsonProcessingException e) {
//            log.error("Error occurred during converting intermediate status to domain object", e);
//            throw new ExceptionHandlerUtil(HttpStatus.BAD_REQUEST, "Something went wrong");
//        }
        return login;
    }

    public LoginEntity mapToJpaEntity(Provider<LoginEntity> loginEntityProvider, Login login) throws ExceptionHandlerUtil {
//        modelMapper.typeMap(LoginEntity.class, LoginEntity.class).setProvider(loginEntityProvider);
        LoginEntity loginEntity = modelMapper.map(login, LoginEntity.class);
        return loginEntity;
    }

    List<Login> mapJpaListToDomainEntityList(List<LoginEntity> loginEntityList) throws ExceptionHandlerUtil {
        List<Login> loginList = new ArrayList<Login>();
        for (LoginEntity loginEntity : loginEntityList) {
            Login login = new Login();
            BeanUtils.copyProperties(loginEntity, login);
//            try {
            login = modelMapper.map(loginEntity, Login.class);
//            } catch (JsonProcessingException e) {
//                log.error("Error occurred during converting intermediate status to domain object", e);
//                throw new ExceptionHandlerUtil(HttpStatus.BAD_REQUEST, "Something went wrong");
//            }
            loginList.add(login);
        }
        return loginList;
    }

}
