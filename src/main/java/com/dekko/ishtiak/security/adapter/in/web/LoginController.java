package com.dekko.ishtiak.security.adapter.in.web;

import com.dekko.ishtiak.core.util.Api;
import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.core.util.response.CommonObjectResponseBody;
import com.dekko.ishtiak.security.application.port.in.dto.request.LoginRequest;
import com.dekko.ishtiak.security.application.port.in.LoginFeatureUseCase;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;
@Slf4j
@RequiredArgsConstructor
@CrossOrigin(origins = Api.SERVER_BASE)
@RestController
@RequestMapping(Api.API_BASE + Api.LOGIN_BASE_PATH)
public class LoginController {

    private final LoginFeatureUseCase loginFeatureUseCase;

    @PostMapping(path = "/login")
    public CommonObjectResponseBody getUserLogin(@RequestBody LoginRequest loginRequest) throws ExceptionHandlerUtil {
        try {
            System.out.println("Requsest " + loginRequest);
            CommonObjectResponseBody response = loginFeatureUseCase.userLogin(loginRequest);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }


    @PostMapping(path = "/user-info")
    public CommonObjectResponseBody getUserInfo(@RequestBody LoginRequest loginRequest) throws ExceptionHandlerUtil {
        try {
            System.out.println("Requsest " + loginRequest);
            CommonObjectResponseBody response = loginFeatureUseCase.userInfo(loginRequest);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @PostMapping(path = "/logout")
    public CommonObjectResponseBody getUserLogout(@RequestBody LoginRequest loginRequest) throws ExceptionHandlerUtil {
        try {
            System.out.println("Requsest " + loginRequest);
            CommonObjectResponseBody response = loginFeatureUseCase.userLogout(loginRequest);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

}
