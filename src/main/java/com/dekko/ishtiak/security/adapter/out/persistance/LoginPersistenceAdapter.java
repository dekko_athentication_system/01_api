package com.dekko.ishtiak.security.adapter.out.persistance;

import com.dekko.ishtiak.core.util.IdGenerator;
import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.core.util.repository.UtilRepository;
import com.dekko.ishtiak.security.adapter.out.persistance.database.entity.LoginEntity;
import com.dekko.ishtiak.security.adapter.out.persistance.database.repository.LoginRepository;
import com.dekko.ishtiak.security.application.port.in.dto.request.LoginRequest;
import com.dekko.ishtiak.security.application.port.out.LoginPersistencePort;
import com.dekko.ishtiak.security.domain.Login;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class LoginPersistenceAdapter implements LoginPersistencePort {
    @Autowired
    IdGenerator idGenerator;
    private final ModelMapper modelMapper;
    private final UtilRepository utilRepository;
    private final LoginRepository loginRepository;
    private final LoginModelMapper loginModelMapper;

    @Override
    public Login userLogin(LoginRequest loginRequest) throws ExceptionHandlerUtil {
        LoginEntity entity = loginRepository.getUserInfo(loginRequest);
        if (entity == null) throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Data not found");
        return modelMapper.map(entity, Login.class);
    }

    @Override
    public int updateIsLoggedIn(Login login) throws ExceptionHandlerUtil {
        int entity = loginRepository.updateIsLoggedIn(login);
        System.out.println("Obj2 " + login);
        if (entity == 0) throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Data not found");
        return entity;
    }

    @Override
    public int updateIsLoggedOut(LoginRequest loginRequest) throws ExceptionHandlerUtil {
        int entity = loginRepository.updateIsLoggedOut(loginRequest);
        if (entity == 0) throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Data not found");
        return entity;
    }


    @Override
    public int countIsLoggedIn(LoginRequest loginRequest) throws ExceptionHandlerUtil {
        int entity = loginRepository.countIsLoggedIn(loginRequest);
        if (entity == 0) throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Data not found");
        return entity;
    }

}
