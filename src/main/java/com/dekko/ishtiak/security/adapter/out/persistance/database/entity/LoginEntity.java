package com.dekko.ishtiak.security.adapter.out.persistance.database.entity;


import com.google.gson.GsonBuilder;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@Entity
@Table(name ="login")
public class LoginEntity extends LoginResponseEntity {
    private String userId;
    private String password;

    private String roleOid;
    private String isLoggedIn;

    private String roleId;
    private String roleNameEn;
    private String roleNameBn;
    private String menuJson;

}
