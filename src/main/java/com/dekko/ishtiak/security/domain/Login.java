package com.dekko.ishtiak.security.domain;

import com.dekko.ishtiak.security.adapter.out.persistance.database.entity.LoginResponseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Login extends LoginResponseEntity {
    private String userId;
    private String password;

    private String roleOid;
    private String isLoggedIn;
    private String roleId;
    private String roleNameEn;
    private String roleNameBn;
    private String menuJson;



}
