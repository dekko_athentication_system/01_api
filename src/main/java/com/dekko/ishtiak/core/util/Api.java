package com.dekko.ishtiak.core.util;

public abstract class Api {
	public static final String IMS_CMN_SERVICE_SERVER_PATH = "http://localhost:8080/";
	public static final String IMS_MFI_SERVICE_SERVER_PATH = "http://localhost:8081/";

	//	public static final String SCHOOL_ERP_SERVICE_SERVER_PATH = "http://192.168.0.126:9090/";

	public static final String SERVER_BASE = "*";
	public static final String API_BASE = "/dekko/api";

	public static final String CMN_SERVICE_SERVER_PATH = "http://localhost:8080";

	public static final String REGISTRATION_BASE_PATH = "/v1/registration";

	public static final String LOGIN_BASE_PATH = "/v1/login";

}
