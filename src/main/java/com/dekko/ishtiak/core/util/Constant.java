package com.dekko.ishtiak.core.util;

import com.google.gson.GsonBuilder;
import lombok.Synchronized;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.lang.reflect.Modifier;
import java.math.BigDecimal;


public class Constant {
    public static String REQEUST_PREFIX_DOER = "DOER-";
    public static String REQEUST_PRODUCT_SHIPMENT = "SHIPMENT-";

    // //Liton
    // public static final String PDF_REPORT_PATH =  "/home/liton/User/Project/mra-ims/mra-ims-common-api/service/java/jasperreports/";

    // //Kamal Parvez
    public static final String PDF_REPORT_PATH = "/home/doer/DOER/mra-ims/mra-ims-common-api/service/java/jasperreports/";

    public static final String FILES_STORAGE_ROOT_PATH = "/data/mraims/files"; // For Common file structure

    public static final String CLASS_PATH = "classpath:";
    public static final String JASPER_REPORT_PATH = "jasperreports/";
    public static final String METHOD_POST = "POST";
    public static final String REQUEST_VERSION__DEFAULT = "1.0";
    public static final int REQUEST_TIMEOUT_IN_SECONDS__INFINITY = -1;
    public static final int REQUEST_TIMEOUT_IN_SECONDS__30 = 30;
    public static final int REQUEST_RETRY_COUNT__ZERO = 0;

    public static final String REQEUST_PREFIX_DRWS = "MRA_IMS-";

    public static final String PREFIX_REGISTRATION_ID = "REG ";

    public static final String ACTIVE = "Active";

}
