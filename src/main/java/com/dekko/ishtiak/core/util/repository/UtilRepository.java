package com.dekko.ishtiak.core.util.repository;


public interface UtilRepository {

    String findSchemaByInstituteOid(String instituteOid);

    String findSchemaNameByInstituteOid(String instituteOid);

}
