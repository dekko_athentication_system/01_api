package com.dekko.ishtiak.core.util.repository;


import com.dekko.ishtiak.core.util.DatabaseSchema;
import com.dekko.ishtiak.core.util.Table;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@AllArgsConstructor
@Repository
public class UtilRepositoryImpl implements UtilRepository {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate parameterJdbcTemplate;


    @Override
    public String findSchemaByInstituteOid(String instituteOid) {
        return null;
    }

    @Override
    public String findSchemaNameByInstituteOid(String instituteOid) {
        return null;
    }
}
