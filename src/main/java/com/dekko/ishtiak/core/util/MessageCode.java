package com.dekko.ishtiak.core.util;


public enum MessageCode{

    MSG_2000("MSG_2000", "Data Fetch Successfully"),
    MSG_2001("MSG_2001", "Successfully File or Photo Uploaded"),
    MSG_2002("MSG_2002", "Data Fetch Successfully By Id"),
    MSG_2003("MSG_2003", "Data Fetch Successfully By Oid"),
    MSG_2004("MSG_2004", "Data Not Found"),
    MSG_2005("MSG_2005", "Successfully Submitted Data Saved"),
    MSG_2006("MSG_2006", "Successfully Data Deleted"),
    MSG_2007("MSG_2007", "Successfully Submitted Data Updated"),
    MSG_2009("MSG_2009", "Data Fetch Successfully By Reference Oid"),
    MSG_2010("MSG_2010", "Registration Id Not Found"),
    MSG_2011("MSG_2011", "Wrong Password"),
    MSG_2012("MSG_2012", "Duplicate Registration Id Found"),
    MSG_2013("MSG_2013", "Sorry, The password is too short."),
    MSG_2014("MSG_2014", "Sorry, Your New Password and Confirm Password is not same !!!."),
    MSG_2015("MSG_2015", "User password has been changed successfully."),
    MSG_2016("MSG_2016", "Sorry, Your Old Password and New Password are Same !!!."),
    MSG_2017("MSG_2017", "Sorry, Your Old Password Not Match !!!."),
    MSG_2018("MSG_2018", "User password reset successfully."),
    MSG_2019("MSG_2011", "Successfully Data Locked"),
    MSG_2020("MSG_2012", "Successfully Data Unlocked From Locked"),
    MSG_2021("MSG_2013", "Successfully Data Approved"),
    MSG_2022("MSG_2022", "Your given employee registration id already exists!!!. Please try again by entering another registration id for employee."),
    MSG_2023("MSG_2023", "Your given employee registration id is valid and accepted.");





    private final String code;
    private final String message;

    MessageCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return code + " " + message;
    }

    public static MessageCode getByValue(String code) {
        for(MessageCode messageCode : values()) {
            if(messageCode.code == code) return messageCode;
        }
        throw new IllegalArgumentException("Invalid Message code: " + code);
    }

}
