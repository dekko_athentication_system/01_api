package com.dekko.ishtiak.core.utility.service;

import com.dekko.ishtiak.core.util.IdGenerator;
import com.dekko.ishtiak.core.utility.persistance.port.UtilityPersistencePort;
import com.dekko.ishtiak.core.utility.persistance.repository.UtilityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UtilityPersistenceAdapterService implements UtilityPersistencePort {
    @Autowired
    IdGenerator idGenerator;
    //    private final ModelMapper modelMapper;
    private final UtilityRepository utilityRepository;

    @Override
    public int getTableDataCount(String schemaName, String tableName, String status) {
        return utilityRepository.simpleTableDataCount(schemaName, tableName, status);
    }

    @Override
    public String getFormatIdsForInOperator(List<String> ids) {
        return utilityRepository.convertIdsForInOperator(ids);
    }

    @Override
    public String getSchemaByInstituteOid(String instituteOid) {
        return utilityRepository.findSchemaByInstituteOid(instituteOid);
    }

    @Override
    public String getSchemaNameByInstituteOid(String instituteOid) {
        return utilityRepository.findSchemaNameByInstituteOid(instituteOid);
    }

}
