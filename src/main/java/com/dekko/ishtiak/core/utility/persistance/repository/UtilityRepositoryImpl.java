package com.dekko.ishtiak.core.utility.persistance.repository;


import com.dekko.ishtiak.core.util.DatabaseSchema;
import com.dekko.ishtiak.core.util.Table;
import lombok.AllArgsConstructor;
import lombok.Synchronized;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Repository
public class UtilityRepositoryImpl implements UtilityRepository {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public int simpleTableDataCount(String schemaName, String tableName, String status) {
        String sql = "SELECT COUNT(oid) FROM " + schemaName + "." + tableName + "WHERE 1 = 1";
        if (StringUtils.isNotBlank(status)) {
            sql += " AND status = '" + status + "'";
        }
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public String findSchemaByInstituteOid(String instituteOid) {
        return null;
    }


    @Override
    public String findSchemaNameByInstituteOid(String instituteOid) {
        return findSchemaByInstituteOid(instituteOid).concat(".");
    }

    @Override
    public String convertIdsForInOperator(List<String> ids) {
        String s = "'";
        for (int i = 0; i < ids.size(); i++) {
            if (i != ids.size() - 1) {
                s += "" + ids.get(i) + "','";
            } else if (i == ids.size() - 1) {
                s += "" + ids.get(i) + "'";
            }
        }
        return s;
    }


}
