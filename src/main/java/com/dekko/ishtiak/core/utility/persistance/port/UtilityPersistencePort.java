package com.dekko.ishtiak.core.utility.persistance.port;

import java.util.List;

public interface UtilityPersistencePort {

    int getTableDataCount(String schemaName, String tableName, String status);

    String getSchemaByInstituteOid(String instituteOid);

    String getSchemaNameByInstituteOid(String instituteOid);

    String getFormatIdsForInOperator(List<String> ids);

}
