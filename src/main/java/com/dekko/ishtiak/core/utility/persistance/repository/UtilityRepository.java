package com.dekko.ishtiak.core.utility.persistance.repository;


import java.util.List;
import java.util.Map;


public interface UtilityRepository {

    int simpleTableDataCount(String schemaName, String tableName, String status);

    String findSchemaByInstituteOid(String instituteOid);

    String findSchemaNameByInstituteOid(String instituteOid);

    String convertIdsForInOperator(List<String> ids);

}
