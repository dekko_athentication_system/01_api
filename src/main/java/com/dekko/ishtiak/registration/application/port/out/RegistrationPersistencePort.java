package com.dekko.ishtiak.registration.application.port.out;

import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.registration.adapter.out.persistance.database.entity.RegistrationEntity;
import com.dekko.ishtiak.registration.application.port.in.dto.request.RegistrationRequest;
import com.dekko.ishtiak.registration.domain.Registration;

import java.util.List;
import java.util.Map;

public interface RegistrationPersistencePort {

    int countRegistration();
    Registration findByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil;
    List<Registration> findAllRegistration(Map<String, String> params) throws ExceptionHandlerUtil;

    int countTotalRegistrationForPagination(Map<String, String> params) throws ExceptionHandlerUtil;

    Registration save (Registration registration, Map<String, String> params) throws ExceptionHandlerUtil;
    Registration saveLogin (Registration registration, Map<String, String> params) throws ExceptionHandlerUtil;
    Registration updateRegistration(Registration registration, Map<String, String> params) throws ExceptionHandlerUtil;
    Registration updateRegistrationWithLogin(Registration registration, Map<String, String> params) throws ExceptionHandlerUtil;



}

