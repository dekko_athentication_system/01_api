package com.dekko.ishtiak.registration.application.port.in.dto.request;

import com.google.gson.GsonBuilder;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.*;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegistrationRequest {

    private String oid;
    private String registrationId;
    @NotNull(message = "First Name is required")
    @NotEmpty(message = "First Name can not be empty")
    private String firstName;
    @NotNull(message = "Last Name is required")
    @NotEmpty(message = "Last Name can not be empty")
    private String lastName;
    private String email;
//    @Pattern(regexp = "^01[3-9]\\d{8}$", message = "Contact no is not valid")
    private String mobile;
    private String address;
    private String dateOfBirth;
    private String status;
    private String loginOid;

    private String userId;
    private String password;

    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;

}
