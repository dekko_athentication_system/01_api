package com.dekko.ishtiak.registration.application.service;

import com.dekko.ishtiak.core.util.*;
import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.core.util.repository.UtilRepository;
import com.dekko.ishtiak.core.util.response.CommonListResponseBody;
import com.dekko.ishtiak.core.util.response.CommonObjectResponseBody;
import com.dekko.ishtiak.registration.adapter.out.persistance.database.entity.RegistrationEntity;
import com.dekko.ishtiak.registration.adapter.out.persistance.database.repository.RegistrationRepository;
import com.dekko.ishtiak.registration.application.port.in.RegistrationFeatureUseCase;
import com.dekko.ishtiak.registration.application.port.in.dto.request.RegistrationRequest;
import com.dekko.ishtiak.registration.application.port.out.RegistrationPersistencePort;
import com.dekko.ishtiak.registration.domain.Registration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.dekko.ishtiak.core.util.Constant.ACTIVE;

@RequiredArgsConstructor

@Service
@Slf4j
public class RegistrationService implements RegistrationFeatureUseCase {

    private final ModelMapper modelMapper;
    private final IdGenerator idGenerator;
    private final UtilRepository utilRepository;
    private final RegistrationPersistencePort registrationPersistencePort;
    private final RegistrationRepository registrationRepository;
    private final RestTemplate restTemplate;

    @Override
    public CommonListResponseBody<Object> findAllRegistration(Map<String, String> params) throws ExceptionHandlerUtil {
        try {
            List<Registration> loanList = registrationPersistencePort.findAllRegistration(params);
            int existingLoanCount = registrationPersistencePort.countTotalRegistrationForPagination(params);

            CommonListResponseBody<Object> response;
            if (loanList.isEmpty()) {
                response = CommonListResponseBody.builder().data(new ArrayList<>()).build();
            }else {
                response = CommonListResponseBody.builder().data(Arrays.asList(loanList.toArray())).build();
                response.setStatus(true);
                response.setCode(ResponseCode.OK.getValue());
                response.setMessageCode(MessageCode.MSG_2000.getCode());
                response.setMessage(MessageCode.MSG_2000.getMessage());
                response.setCount(existingLoanCount);
            }
            return response;
        }catch(Exception ex){
            ex.printStackTrace();
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Registration data not found");
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CommonObjectResponseBody saveRegistration(RegistrationRequest request, Map<String, String> params) throws ExceptionHandlerUtil {
        try {
            Registration registration = null;

            int count = registrationPersistencePort.countRegistration();
            request.setStatus(Constant.ACTIVE);
            request.setRegistrationId(idGenerator.generateRandormNumber(count));
            Registration requested = mapRequestToDomain(request, params);
            Registration login = registrationPersistencePort.saveLogin(requested, params);
            if(login != null){
               registration = registrationPersistencePort.save(requested, params);
            }
            if (registration == null)
                throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Registration data not saved");
            CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(registration).build();
            response.setStatus(true);
            response.setCode(ResponseCode.CREATED.getValue());
            response.setMessageCode(MessageCode.MSG_2005.getCode());
            response.setMessage(MessageCode.MSG_2005.getMessage());
            return response;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Registration data not Save");
        }
    }

    @Override
    public CommonObjectResponseBody findOfficeByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil {
        Registration office = registrationPersistencePort.findByOid(oid, params);
        if (office == null) throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Registration data not found by this oid: " + oid);
        CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(office).build();
        response.setStatus(true);
        response.setCode(ResponseCode.OK.getValue());
        response.setMessageCode(MessageCode.MSG_2003.getCode());
        response.setMessage(MessageCode.MSG_2003.getMessage());
        return response;
    }

    @Override
    public CommonObjectResponseBody updateRegistration(RegistrationRequest request, String oid, Map<String, String> params) throws ExceptionHandlerUtil {
        try{
            Registration registration = null;
            Registration registrationLogin = null;
            Registration existLoan = registrationPersistencePort.findByOid(oid, params);
            if (existLoan != null) {
                request.setOid(oid);
                registration = registrationPersistencePort.updateRegistration(mapRequestToDomain(request, params), params);
                registrationLogin = registrationPersistencePort.updateRegistrationWithLogin(mapRequestToDomain(request, params), params);
            }
            if (registration == null && registrationLogin == null) throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Registration data not updated");
            CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(registration).build();
            response.setStatus(true);
            response.setCode(ResponseCode.CREATED.getValue());
            response.setMessageCode(MessageCode.MSG_2007.getCode());
            response.setMessage(MessageCode.MSG_2007.getMessage());

            return response;
        } catch(Exception e){
            e.printStackTrace();
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Registration data not found");
        }
    }

    private Registration mapRequestToDomain(RegistrationRequest request, Map<String, String> params) throws ExceptionHandlerUtil {
        Registration existRegistration;
        if (request.getOid() != null) {
            existRegistration = registrationPersistencePort.findByOid(request.getOid(),  params);
            request.setCreatedBy(existRegistration.getCreatedBy());
            request.setCreatedOn(existRegistration.getCreatedOn());
            request.setUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
            request.setUpdatedBy(request.getUpdatedBy());
        } else {
            request.setCreatedBy(request.getCreatedBy());
            request.setCreatedOn(request.getCreatedOn() == null ? Timestamp.valueOf(LocalDateTime.now()) : request.getCreatedOn());
        }
        Registration registration = modelMapper.map(request, Registration.class);
        return registration;
    }

}

