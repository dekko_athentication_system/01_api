package com.dekko.ishtiak.registration.application.port.in;


import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.core.util.response.CommonListResponseBody;
import com.dekko.ishtiak.core.util.response.CommonObjectResponseBody;
import com.dekko.ishtiak.registration.application.port.in.dto.request.RegistrationRequest;

import java.util.Map;

public interface RegistrationFeatureUseCase {

    CommonListResponseBody<Object> findAllRegistration(Map<String, String> params) throws ExceptionHandlerUtil;
    CommonObjectResponseBody saveRegistration(RegistrationRequest request, Map<String, String> params) throws ExceptionHandlerUtil;
    CommonObjectResponseBody findOfficeByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil;

    CommonObjectResponseBody updateRegistration (RegistrationRequest request, String oid, Map<String, String> params) throws ExceptionHandlerUtil;

}
