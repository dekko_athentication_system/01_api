package com.dekko.ishtiak.registration.adapter.out.persistance.database.repository;


import com.dekko.ishtiak.core.util.Table;
import com.dekko.ishtiak.registration.adapter.out.persistance.database.entity.RegistrationEntity;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Repository
public class RegistrationRepositoryImpl implements RegistrationRepository {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public int countRegistration() {
        String sql = "Select count(*) from " + Table.REGISTRATION;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public int countTotalLoanForPagination(Map<String, String> params) {
        String sql = "select count(*) "
                + " from " + Table.REGISTRATION + " r "
                + " where 1 = 1 ";

        if (StringUtils.isNotBlank(params.get("searchText"))) {
            sql += " and (r.registration_id ilike '%" + params.get("searchText").trim() + "%'"
                    + " or r.first_name ilike '%" + params.get("searchText").trim() + "%'"
                    + " or r.last_name ilike '%" + params.get("searchText").trim() + "%'"
                    + " or r.email ilike '%" + params.get("searchText").trim() + "%'"
                    + " or r.mobile ilike '%" + params.get("searchText").trim() + "%')";
        }

        return jdbcTemplate.queryForObject(sql, Integer.class);

    }


    @Override
    public List<RegistrationEntity> findAllRegistration(Map<String, String> params) {
        List<RegistrationEntity> dataList = new ArrayList<>();

        String sql = "SELECT r.oid, r.registration_id, r.first_name, r.last_name, r.email, r.mobile, r.address, " +
                " r.date_of_birth, r.status, r.login_oid, r.created_by, r.created_on, r.updated_by, r.updated_on " +
                " from " + Table.REGISTRATION + " r " +
                " where 1 = 1 ";


        if (StringUtils.isNotBlank(params.get("searchText"))) {
            sql += " and (r.registration_id ilike '%" + params.get("searchText").trim() + "%'"
                    + " or r.first_name ilike '%" + params.get("searchText").trim() + "%'"
                    + " or r.last_name ilike '%" + params.get("searchText").trim() + "%'"
                    + " or r.email ilike '%" + params.get("searchText").trim() + "%'"
                    + " or r.mobile ilike '%" + params.get("searchText").trim() + "%')";
        }

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            RegistrationEntity data = new RegistrationEntity();

            data.setOid((String) row.get("oid"));
            data.setRegistrationId((String) row.get("registration_id"));
            data.setFirstName((String) row.get("first_name"));
            data.setLastName((String) row.get("last_name"));
            data.setEmail((String) row.get("email"));
            data.setMobile((String) row.get("mobile"));
            data.setAddress((String) row.get("address"));
            data.setDateOfBirth((String) row.get("date_of_birth"));
            data.setStatus((String) row.get("status"));
            data.setLoginOid((String) row.get("login_oid"));
            data.setCreatedBy((String) row.get("created_by"));
            data.setCreatedOn((Date) row.get("created_on"));
            data.setUpdatedBy((String) row.get("updated_by"));
            data.setUpdatedOn((Date) row.get("updated_on"));

            dataList.add(data);
        }

        return dataList;
    }

    @Override
    public int saveWithSchema(RegistrationEntity entity, Map<String, String> params){

        String sql = "INSERT INTO " + Table.REGISTRATION + "(" +
                " oid, registration_id, first_name, last_name, email, mobile, address," +
                " date_of_birth, status, login_oid" +
                ") VALUES (" +
                " ?, ?, ?, ?, ?, ?, ?," +
                " ?, ?, ?)";

        return jdbcTemplate.update(sql,
                entity.getOid(),
                entity.getRegistrationId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getEmail(),
                entity.getMobile(),
                entity.getAddress(),

                entity.getDateOfBirth(),
                entity.getStatus(),
                entity.getLoginOid());
    };

    @Override
    public int saveLogin(RegistrationEntity entity, Map<String, String> params){

        String sql = "INSERT INTO " + Table.LOGIN + "(" +
                " oid, user_id, password, role_oid, status, is_logged_in, created_by" +
                ") VALUES (" +
                " ?, ?, ?, ?, ?, ?, ?)";

        return jdbcTemplate.update(sql,
                entity.getOid(),
                entity.getUserId(),
                entity.getPassword(),
                "Role-Oid-002",
                entity.getStatus(),
                "No",
                entity.getCreatedBy());
    };


    @Override
    public RegistrationEntity findByOid(String oid, Map<String, String> params) {
        RegistrationEntity data = new RegistrationEntity();
        try {
            String sql = "SELECT r.oid, r.registration_id, r.first_name, r.last_name, r.email, r.mobile, r.address, " +
                    " r.date_of_birth, r.status, r.login_oid, r.created_on, r.updated_by, r.updated_on " +
                    " from " + Table.REGISTRATION + " r " +
                    " where 1 = 1  and  r.oid = '" + oid + "'";

            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

            for (Map<String, Object> row : rows) {
                data.setOid((String) row.get("oid"));
                data.setRegistrationId((String) row.get("registration_id"));
                data.setFirstName((String) row.get("first_name"));
                data.setLastName((String) row.get("last_name"));
                data.setEmail((String) row.get("email"));
                data.setMobile((String) row.get("mobile"));
                data.setAddress((String) row.get("address"));
                data.setDateOfBirth((String) row.get("date_of_birth"));
                data.setStatus((String) row.get("status"));
                data.setLoginOid((String) row.get("login_oid"));
                data.setCreatedBy((String) row.get("created_by"));
                data.setCreatedOn((Date) row.get("created_on"));
                data.setUpdatedBy((String) row.get("updated_by"));
                data.setUpdatedOn((Date) row.get("updated_on"));

                break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public int updateRegistration(RegistrationEntity entity, Map<String, String> params) {
        try {
            String sql = "UPDATE " + Table.REGISTRATION +
                    " SET first_name = ?, last_name = ?, email = ?, mobile = ?, " +
                    " address = ?, date_of_birth = ?" +
                    " where oid = ?";

            return jdbcTemplate.update(sql,
                    entity.getFirstName(),
                    entity.getLastName(),
                    entity.getEmail(),
                    entity.getMobile(),
                    entity.getAddress(),
                    entity.getDateOfBirth(),
                    entity.getOid());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int updateRegistrationWithLogin(RegistrationEntity entity, Map<String, String> params) {
        try {
            String sql = "UPDATE " + Table.LOGIN +
                    " SET user_id = ?, password = ?" +
                    " where oid = ?";

            return jdbcTemplate.update(sql,
                    entity.getUserId(),
                    entity.getPassword(),
                    entity.getOid());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}
