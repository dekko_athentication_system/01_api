package com.dekko.ishtiak.registration.adapter.out.persistance;

import com.dekko.ishtiak.registration.adapter.out.persistance.database.entity.RegistrationEntity;
import com.dekko.ishtiak.registration.domain.Registration;
import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class RegistrationModelMapper {

    //    private final ObjectMapper objectMapper;
    private final ModelMapper modelMapper;

    public RegistrationModelMapper(
//            ObjectMapper objectMapper,
            ModelMapper modelMapper) {
//        this.objectMapper = objectMapper;
        this.modelMapper = modelMapper;
    }

    public Registration mapToDomainEntity(RegistrationEntity registrationEntity, Class<Registration> loginClass) throws ExceptionHandlerUtil {
        /*modelMapper.typeMap(RegistrationEntity.class, Registration.class)
                .addMappings(mapper -> mapper.map(RegistrationEntity::getBranchId, Registration::setBranchId));
        modelMapper.typeMap(RegistrationEntity.class, Registration.class)
                .addMappings(mapper -> mapper.map(RegistrationEntity::getOid, Registration::setOid));*/
        Registration login = modelMapper.map(registrationEntity, Registration.class);

//        try {
//            login.setStatus(objectMapper.readValue(registrationEntity.getStatus(), String.class));
//        } catch (JsonProcessingException e) {
//            log.error("Error occurred during converting intermediate status to domain object", e);
//            throw new ExceptionHandlerUtil(HttpStatus.BAD_REQUEST, "Something went wrong");
//        }
        return login;
    }

    public RegistrationEntity mapToJpaEntity(Provider<RegistrationEntity> loginEntityProvider, Registration login) throws ExceptionHandlerUtil {
//        modelMapper.typeMap(RegistrationEntity.class, RegistrationEntity.class).setProvider(loginEntityProvider);
        RegistrationEntity registrationEntity = modelMapper.map(login, RegistrationEntity.class);
        return registrationEntity;
    }

    List<Registration> mapJpaListToDomainEntityList(List<RegistrationEntity> registrationEntityList) throws ExceptionHandlerUtil {
        List<Registration> loginList = new ArrayList<Registration>();
        for (RegistrationEntity registrationEntity : registrationEntityList) {
            Registration login = new Registration();
            BeanUtils.copyProperties(registrationEntity, login);
//            try {
            login = modelMapper.map(registrationEntity, Registration.class);
//            } catch (JsonProcessingException e) {
//                log.error("Error occurred during converting intermediate status to domain object", e);
//                throw new ExceptionHandlerUtil(HttpStatus.BAD_REQUEST, "Something went wrong");
//            }
            loginList.add(login);
        }
        return loginList;
    }

}
