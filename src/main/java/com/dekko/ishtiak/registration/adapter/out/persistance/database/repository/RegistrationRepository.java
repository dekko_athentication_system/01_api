package com.dekko.ishtiak.registration.adapter.out.persistance.database.repository;


import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.registration.adapter.out.persistance.database.entity.RegistrationEntity;

import java.util.List;
import java.util.Map;


public interface RegistrationRepository {

    int countRegistration();
    List<RegistrationEntity> findAllRegistration(Map<String, String> params);
    int countTotalLoanForPagination(Map<String, String> params) throws ExceptionHandlerUtil;
    int saveWithSchema(RegistrationEntity entity, Map<String, String> params);
    int saveLogin(RegistrationEntity entity, Map<String, String> params);
    RegistrationEntity findByOid(String oid, Map<String, String> params);
    int updateRegistration(RegistrationEntity entity, Map<String, String> params);
    int updateRegistrationWithLogin(RegistrationEntity entity, Map<String, String> params);
}
