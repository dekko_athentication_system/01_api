package com.dekko.ishtiak.registration.adapter.out.persistance;

import com.dekko.ishtiak.core.util.IdGenerator;
import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.core.util.repository.UtilRepository;
import com.dekko.ishtiak.registration.adapter.out.persistance.database.entity.RegistrationEntity;
import com.dekko.ishtiak.registration.adapter.out.persistance.database.repository.RegistrationRepository;
import com.dekko.ishtiak.registration.application.port.out.RegistrationPersistencePort;
import com.dekko.ishtiak.registration.domain.Registration;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class RegistrationPersistenceAdapter implements RegistrationPersistencePort {
    @Autowired
    IdGenerator idGenerator;
    private final ModelMapper modelMapper;
    private final UtilRepository utilRepository;
    private final RegistrationRepository registrationRepository;
    private final RegistrationModelMapper registrationModelMapper;

    @Override
    public int countRegistration() {
        return registrationRepository.countRegistration();
    }

    @Override
    public int countTotalRegistrationForPagination(Map<String, String> params) throws ExceptionHandlerUtil {
        return registrationRepository.countTotalLoanForPagination(params);
    }

    @Override
    public List<Registration> findAllRegistration(Map<String, String> params) throws ExceptionHandlerUtil {
        List<RegistrationEntity> entityList = registrationRepository.findAllRegistration(params);
        return entityList.size() > 0 ? entityList.stream().map(entity -> modelMapper.map(entity, Registration.class)).collect(Collectors.toList()) : new ArrayList<>();
    }

    @Override
    public Registration save(Registration registration, Map<String, String> params) throws ExceptionHandlerUtil {
        Provider<RegistrationEntity> loanEntityProvider = provider -> new RegistrationEntity();
        RegistrationEntity entity = registrationModelMapper.mapToJpaEntity(loanEntityProvider, registration);
        entity.setOid(idGenerator.generateOid());
        int saveCount = registrationRepository.saveWithSchema(entity, params);
        return saveCount > 0 ? modelMapper.map(entity, Registration.class) : null;
    }

    @Override
    public Registration saveLogin(Registration registration, Map<String, String> params) throws ExceptionHandlerUtil {
        Provider<RegistrationEntity> loanEntityProvider = provider -> new RegistrationEntity();
        RegistrationEntity entity = registrationModelMapper.mapToJpaEntity(loanEntityProvider, registration);
        entity.setOid(idGenerator.generateOid());
        int saveCount = registrationRepository.saveLogin(entity, params);
        return saveCount > 0 ? modelMapper.map(entity, Registration.class) : null;
    }

    @Override
    public Registration findByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil {
        RegistrationEntity entity = registrationRepository.findByOid(oid, params);
        if (entity == null) throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Data not found");
        return modelMapper.map(entity, Registration.class);
    }

    @Override
    public Registration updateRegistration(Registration registration, Map<String, String> params) throws ExceptionHandlerUtil {
        RegistrationEntity entity = modelMapper.map(registration, RegistrationEntity.class);
        int updateCount = registrationRepository.updateRegistration(entity, params);
        return updateCount > 0 ? modelMapper.map(entity, Registration.class) : null;
    }

    @Override
    public Registration updateRegistrationWithLogin(Registration registration, Map<String, String> params) throws ExceptionHandlerUtil {
        RegistrationEntity entity = modelMapper.map(registration, RegistrationEntity.class);
        int updateCountLogin = registrationRepository.updateRegistrationWithLogin(entity, params);
        return updateCountLogin > 0 ? modelMapper.map(entity, Registration.class) : null;
    }

}
