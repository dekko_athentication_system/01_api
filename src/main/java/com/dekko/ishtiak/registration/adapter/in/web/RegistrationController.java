package com.dekko.ishtiak.registration.adapter.in.web;

import com.dekko.ishtiak.core.util.Api;
import com.dekko.ishtiak.core.util.exception.ExceptionHandlerUtil;
import com.dekko.ishtiak.core.util.response.CommonListResponseBody;
import com.dekko.ishtiak.core.util.response.CommonObjectResponseBody;
import com.dekko.ishtiak.registration.application.port.in.RegistrationFeatureUseCase;
import com.dekko.ishtiak.registration.application.port.in.dto.request.RegistrationRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;
@Slf4j
@RequiredArgsConstructor
@CrossOrigin(origins = Api.SERVER_BASE)
@RestController
@RequestMapping(Api.API_BASE + Api.REGISTRATION_BASE_PATH)
public class RegistrationController {

    private final RegistrationFeatureUseCase registrationFeatureUseCase;

    @GetMapping(path = "/list")
    public CommonListResponseBody getRegistrationList(@RequestParam Map<String, String> params) throws ExceptionHandlerUtil {
        try {
            CommonListResponseBody response = registrationFeatureUseCase.findAllRegistration(params);
            log.info("List response sent for loan: {}", response);

            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }


    @PostMapping(path = "/save")
        public CommonObjectResponseBody saveLoan(@Valid @RequestBody RegistrationRequest request, @RequestParam Map<String, String> params) {
        try {
            log.info("Request got for save loan: {}", request);
            CommonObjectResponseBody response = registrationFeatureUseCase.saveRegistration(request, params);
            log.info("Save response sent for loan: {}", response);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @PostMapping(path = "/{oid}")
    public CommonObjectResponseBody getOfficeByOid(@PathVariable String oid, @RequestParam Map<String, String> params) throws ExceptionHandlerUtil {
        try {
            CommonObjectResponseBody response = registrationFeatureUseCase.findOfficeByOid(oid, params);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @PostMapping(path = "/edit/{oid}")
    public CommonObjectResponseBody updateRegistration(@Valid @RequestBody RegistrationRequest request, @PathVariable String oid, @RequestParam Map<String, String> params) {
        try {
            log.info("Request got for Registration update: {}", request);
            CommonObjectResponseBody response = registrationFeatureUseCase.updateRegistration(request, oid, params);
            log.info("Update response sent for Registration : {}", response);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

}
