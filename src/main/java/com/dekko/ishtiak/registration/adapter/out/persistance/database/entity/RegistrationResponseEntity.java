package com.dekko.ishtiak.registration.adapter.out.persistance.database.entity;

import com.dekko.ishtiak.core.util.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationResponseEntity extends BaseEntity {

    private String oid;
    private String registrationId;
    private String firstName;
    private String lastName;
    private String email;
    private String mobile;
    private String address;
    private String dateOfBirth;
    private String status;
    private String loginOid;
    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;
}
