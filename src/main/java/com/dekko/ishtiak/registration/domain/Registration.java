package com.dekko.ishtiak.registration.domain;

import com.dekko.ishtiak.registration.adapter.out.persistance.database.entity.RegistrationResponseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Registration extends RegistrationResponseEntity {

    private String oid;
    private String registrationId;
    private String firstName;
    private String lastName;
    private String email;
    private String mobile;
    private String address;
    private String dateOfBirth;
    private String status;
    private String loginOid;
    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;

    private String userId;
    private String password;


}
